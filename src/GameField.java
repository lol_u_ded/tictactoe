/*
 * Copyright (c) CyberNord 2022, All rights reserved.
 */

public class GameField implements Game {

    public enum GameStatus {
        RUNNING, X_WON, O_WON, DRAW
    }

    public GameStatus gameStatus;
    private int counterTurn;                   // this integer will count the rounds, max rounds are 9.
    private final char[][] gameField;
    private boolean xTurn;                    // turn counter xTurn false "O"-Turn
    private final int ROWS = 3;
    private final int COLS = 3;

    public GameField(boolean StartingPlayer) {
        gameField = new char[ROWS][COLS];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                gameField[i][j] = ' ';
            }
        }
        xTurn = StartingPlayer;
        counterTurn = 1;
    }

    /**
     * Identifies the current player.
     * @return Player 'O' if !xTurn and X if xTurn
     */
    @Override
    public Character currentPlayer(){
        char Player;
        if (xTurn) {
            Player = 'X';
        }else {
            Player = 'O';
        }
        return Player;
    }

    /**
     * Checks if the values are valid.
     * Checks if the given position is valid according to the CharArray dimension.
     * @param row   integer position identifier row.
     * @param col   integer position identifier column.
     * @return      a true if the integer position identifier row values are correct.
     * @throws InvalidInputException    throws Exception if the position does not exist in current CharArray.
     */
    @Override
    public boolean checkInput(int row, int col) throws InvalidInputException {
        if(row >= 3 || col >= 3 || row < 0 || col < 0 ){
            throw new InvalidInputException (row + 1 , col + 1);
        }
        return true;
    }

    /**
     * Checks if there is already a token.
     * It will check if at the given position is a token. Basically if the Char in the CharArray is something else than ' ' it will say it is not empty.
     * @param row   integer position identifier row.
     * @param col   integer position identifier column.
     * @return      true if the position is free.
     * @throws AlreadyTakenException        If there is already a token (either 'O' or 'X')  it will throw this Exception.
     */
    @Override
    public boolean checkEmpty(int row, int col) throws AlreadyTakenException{
        if (gameField[row][col] == ' ') {
            return true;
        } else {
            throw new AlreadyTakenException(row + 1 , col + 1);
        }
    }

    /**
     * Checks win situation in columns.
     * @param Player    The actual player either 'O' or  'X'
     * @return          True if there is a win situation
     */
    @Override
    public boolean checkCols(char Player){
        for (int i = 0; i <= 2; i++) {
            if (gameField[0][i] == Player && gameField[1][i] == Player && gameField[2][i] == Player)
                return true;
        }
        return false;
    }

    /**
     * Checks win situation in rows.
     * @param Player    The actual player either 'O' or  'X'
     * @return          True if there is a win situation
     */
    @Override
    public boolean checkRows(char Player){
        for (int i = 0; i <= 2; i++) {
            if (gameField[i][0] == Player && gameField[i][1] == Player && gameField[i][2] == Player)
                return true;
        }
        return false;
    }

    /**
     * Checks win situation in diagonals.
     * @param Player    The actual player either 'O' or  'X'
     * @return          True if there is a win situation
     */
    @Override
    public boolean checkDia(char Player){
        if (gameField[0][0] == Player && gameField[1][1] == Player && gameField[2][2] == Player)
            return true;
        else if (gameField[0][2] == Player && gameField[1][1] == Player && gameField[2][0] == Player) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Method checks if there is a win situation.
     * It will call some sub methods and will refresh GameStatus.
     */
    @Override
    public void refreshStatus() {
        if (checkRows(currentPlayer())){            // checks if win condition in row
            if(currentPlayer() == 'X') {
                gameStatus = GameStatus.X_WON;
            }else{
                gameStatus = GameStatus.O_WON;
            }
        }else if(checkCols(currentPlayer())){       // checks if win condition in col
            if(currentPlayer() == 'X') {
                gameStatus = GameStatus.X_WON;
            }else{
                gameStatus = GameStatus.O_WON;
            }
        }else if(checkDia(currentPlayer())) {       // checks if win condition in diagonals
            if(currentPlayer() == 'X') {
                gameStatus = GameStatus.X_WON;
            }else{
                gameStatus = GameStatus.O_WON;
            }
        }else if ( counterTurn == 9) {              // checks if maximum
            gameStatus = GameStatus.DRAW;
        }else{
            gameStatus = GameStatus.RUNNING;
        }
    }

    /**
     * Method changes the Players turn.
     */
    @Override
    public void changePlayer(){
        if (xTurn){
            xTurn = false;
        }else{
            xTurn = true;
        }
        counterTurn++;
    }

    /**
     * Sets a token on given position.
     * It will set the token 'O' if circles turn and token 'X' if crosses turn.
     * @param row   integer position identifier row.
     * @param col   integer position identifier column.
     * @return      True if placing the token was successful
     * @throws TicTacToeException   If it is not possible it will throw an exception.
     */
    @Override
    public boolean setToken(int row, int col) throws TicTacToeException {
        if (!checkInput(row, col) || !checkEmpty(row, col)) {
            throw new TicTacToeException("This input [" + (row+1) +"|" + (col+1) + "] is not possible \n");
        }else if (xTurn){
            gameField[row][col] = 'X';
            return true;
        }else{       // !xTurn
            gameField[row][col] = 'O';
            return true;
        }
    }

    /**
     * Prints out actual GameField.
     */
    @Override
    public void print() {
        Out.println(this.toString());
    }

    /**
     * Sub method to cast a row into a String.
     * @param row   integer position identifier row.
     */
    @Override
    public String displayRow(int row){
        return (row + 1 + "| " + gameField[row][0] + " | " + gameField[row][1] + " | " + gameField[row][2] + " | " + "\n");
    }

    /**
     * Builds a String that contains the actual GameField.
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder a = new StringBuilder();
        a.append("Round: " + counterTurn + "\n==============\n");
        a.append("   1   2   3 \n");
        a.append(displayRow(0) + "  ------------\n" + displayRow(1) +  "  ------------\n" + displayRow(2));
        return a.toString();
    }
}
