/*
 * Copyright (c) CyberNord 2022, All rights reserved.
 */

public class GameLoop {

    /**
     * Random boolean value.
     * @return  random boolean value.
     */
    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }

    /**
     * Method Loop to play TicTacToe.
     * It will start with a random player, either X(cross) or O(circle).
     * The positions have to be entered as follows row then column.
     * It will tell you if the position is already taken or does not exist.
     * If there is no winner within 9 rounds it will be a draw.
     */
    public static void playGame(){
        GameField game = new GameField(getRandomBoolean());
        game.print();
        Out.println(" Good Luck! \n Starting Player : " + game.currentPlayer());
        game.refreshStatus();
        while (game.gameStatus == GameField.GameStatus.RUNNING){
            Out.println(" Enter Position were Token should be set :");
            int row = In.readInt()-1;
            int col = In.readInt()-1;
            try {
                game.setToken(row,col);
                game.print();
                game.refreshStatus();
                if (game.gameStatus == GameField.GameStatus.DRAW){
                    Out.println ("Seems like we got no winner in this round. \nIt's a draw!");
                }else if(game.gameStatus == GameField.GameStatus.O_WON){
                    Out.println("The player with token \"O\" is winner. \nWell played!");
                }else if(game.gameStatus == GameField.GameStatus.X_WON) {
                    Out.println("The player with token \"X\" is winner. \nWell played!");
                }else{
                    game.changePlayer();
                    Out.formatln("It is Player %s turn ", game.currentPlayer());
                }
            } catch (TicTacToeException e) {
                Out.println(e.getMessage());
            }
        }
    }
}
