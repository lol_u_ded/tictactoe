/*
 * Copyright (c) CyberNord 2022, All rights reserved.
 */

public class InvalidInputException extends TicTacToeException {

    /**
     * Exception for Array positions that does not exist.
     * @param row integer to identify the row (original value).
     * @param col integer to identify the column (original value).
     */
    public InvalidInputException(int row, int col) {
        super("This input [" + row + "|" + col+ "] is not possible, because that position does not exist. \n");
    }
}
