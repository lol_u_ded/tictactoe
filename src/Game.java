/*
 * Copyright (c) CyberNord 2022, All rights reserved.
 */

public interface Game {

    /**
     * Method shows current player.
     * @return  current player
     */
    Character currentPlayer();

    /**
     *  Checks if the values are valid.
     * @param row   integer position identifier row.
     * @param col   integer position identifier row.
     * @return      true if okay
     * @throws InvalidInputException if false
     */
    boolean checkInput(int row, int col) throws InvalidInputException;

    /**
     * Checks if there is already a token.
     * @param row   integer position identifier row.
     * @param col   integer position identifier column.
     * @return      true if the position is free.
     * @throws AlreadyTakenException    if there is a token.
     */
    boolean checkEmpty(int row, int col) throws AlreadyTakenException;

    /**
     *  Checks win situation in columns.
     * @param Player    current Player
     * @return          true if there is a win situation
     */
    boolean checkCols(char Player);

    /**
     *  Checks win situation in rows.
     * @param Player    current Player
     * @return          true if there is a win situation
     */
    boolean checkRows(char Player);

    /**
     *  Checks win situation in diagonals.
     * @param Player    current Player
     * @return          true if there is a win situation
     */
    boolean checkDia(char Player);

    /**
     * refreshes the actual game status
     */
    void refreshStatus();

    /**
     * Changes player round
     */
    void changePlayer();

    /**
     * Sets a token on given position.
     * @param row   integer position identifier row.
     * @param col   integer position identifier column.
     * @return      true if the placement was successful.
     * @throws TicTacToeException if there was an Error.
     */
    boolean setToken(int row, int col) throws TicTacToeException;

    /**
     * Prints actual game field
     */
    void print();

    /**
     * Casts row into a String
     * @param row   integer position identifier row.
     * @return      String
     */
    String displayRow(int row);
}
