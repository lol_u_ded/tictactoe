/*
 * Copyright (c) CyberNord 2022, All rights reserved.
 */

public class TicTacToeException extends Exception {

    /**
     * General TicTacToe Exception.
     * @param message   Error message.
     */
    public TicTacToeException (String message){
        super(message);
    }
}
