/*
 * Copyright (c) CyberNord 2022, All rights reserved.
 */

public class AlreadyTakenException extends TicTacToeException {

    /**
     * Exception if the array position is already taken.
     * @param row   integer to identify the row (original value).
     * @param col   integer to identify the column (original value).
     */
    public AlreadyTakenException(int row, int col) {
        super("This input [" + (row + 1) + "|" + (col + 1) + "] is not possible, because there is already a token. \n");
    }
}
